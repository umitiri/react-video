import React from 'react';
import Proptypes from "prop-types";
import MovieCard from './MovieCard';
import {Grid} from 'semantic-ui-react';
import {HashLoader} from 'react-spinners';

const MoviesList = ({ movies, deleteMovie }) => {
    const emptyMessage = (
       <p> There are now film </p>
    );
const moviesList = (
    <div>
        <HashLoader
        size={40}
        color={'#36bdb3'}
        loading={movies.fetching}
        />
        {
            movies.error.response ? <h3>Error retrieving data</h3> 
            : 
            <Grid stackable columns={3}>
            {
                movies.movieList.map(movie => <MovieCard
                     key={movie._id} 
                     deleteMovie={deleteMovie} 
                     movie={movie}/>)
            }
            </Grid>
        }
    </div>
    );
        return (
            <div>
                { movies.lenghth === 0 ? emptyMessage : moviesList }
            </div>
        )  
};

MoviesList.propTypes = {
    movies: Proptypes.shape({
        movieList: Proptypes.array.isRequired,
    }).isRequired
};
MoviesList.defaultProps = {}; 
export default MoviesList;