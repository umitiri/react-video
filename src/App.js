import React,{Component} from 'react';
import './App.css';
import { Container} from 'semantic-ui-react'
import MoviesPage from './components/pages/MoviesPage';
import NewMoviePage from './components/pages/NewMoviePage';
import { Route } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import Footer from './components/Footer';
import Header from './components/Header';
class  App extends Component {




  render(){

  return (
    <div className="App">
       
        {/* Attaching the top menu is a simple operation, we only switch `fixed` prop and add another style if it has
            gone beyond the scope of visibility
          */}
          <Header/>
       
        <Container text>
          
      <Route path='/movies' component={MoviesPage} exact></Route>
      <Route path='/movies/new' component={NewMoviePage} exact></Route>
      <Route path='/movie/:_id' component={NewMoviePage} exact></Route>
        </Container>

       <Footer/>
       

    </div>
  );
}
}
export default App;
